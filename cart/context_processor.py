from cart.cart import Cart

def importe_total(request):
    total = 0.0 

    if request.user.is_authenticated:

        if 'cart' in request.session:
            quantityArt = []
            for key ,value in request.session['cart'].items() :

                quantityArt.append(value['quantity'])
                sumArt = sum(quantityArt)

                subtotal = total +(float(value['price'])*float(value['quantity']))
                total = round(subtotal,2)
        # else:
        #     for key ,value in request.session['cart'].items():
        #         subtotal = total +(float(value['price'])*float(value['quantity']))
        #         total = round(subtotal,2)

    return {'importe_total':total}
