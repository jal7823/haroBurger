from django.shortcuts import render,redirect,reverse
from django.contrib.auth.decorators import login_required
from django.views.generic import ListView,DetailView
from django.urls import resolve
from menu.models import Product
from .cart import Cart
from django.contrib import messages
from django.core.paginator import Paginator
from menu.models import Product


# CARRITO
def add_product(request,product_id):


    cart = Cart(request)
    product = Product.objects.get(id= product_id)
    cart.add(product=product)
    convertido = str (product_id)
    messages.success(request,'Product agregado al carrrito!')

    # capura el url antes de agregar al carrito y redireccionamos alli

    try:
        urlAnterior = request.META.get('HTTP_REFERER')
    except :
        urlAnterior = 'index'

    return redirect(urlAnterior)

def remove_product(request,product_id):
    cart = Cart(request)
    product = Product.objects.get(id= product_id)
    cart.remove(product=product)

    try:
        urlAnterior = request.META.get('HTTP_REFERER')
    except :
        urlAnterior = 'index'

    return redirect(urlAnterior)
    
def decrement_product(request,product_id):
    cart = Cart(request)
    product = Product.objects.get(id= product_id)
    cart.decrement(product=product)
    messages.success(request,'Product eliminado del carrito!')
    try:
        urlAnterior = request.META.get('HTTP_REFERER')
    except :
        urlAnterior = 'index'

    return redirect(urlAnterior)

def clear_cart(request):
    cart = Cart(request)
    cart.clear()
    try:
        urlAnterior = request.META.get('HTTP_REFERER')
    except :
        urlAnterior = 'index'

    return redirect(urlAnterior)

def theCart(request):
    return render(request,'cart/theCart.html')

class ListCart(ListView):
    model = Product
    template_name = 'cart/listCart.html'



