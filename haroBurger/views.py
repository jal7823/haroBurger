#django imports
from django.shortcuts import render,redirect
from django.urls import reverse,resolve
from django.db.models import Q
from django.http import Http404, HttpResponseRedirect
#local import
from menu.models import Product


def base(request):
    return render(request,'base.html')

def index(request):
    hamburger = Product.objects.filter(category='hamburgesa')
    hotdogs = Product.objects.filter(category='perros calientes')
    pepitos = Product.objects.filter(category='pepitos')


    context = {
        'hamburger':hamburger,
        'hotdogs':hotdogs,
        'pepitos':pepitos,
    }
    return render(request,'index.html',context)

