from django.contrib import admin
from .models import Ingredient,Product

# Register your models here.


class IngredientAdmin(admin.ModelAdmin):
    list_display = ('name',)


class ProductAdmin(admin.ModelAdmin):
    list_display = ('name',)
    

    

admin.site.register(Ingredient,IngredientAdmin)
admin.site.register(Product,ProductAdmin)
