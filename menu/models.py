from django.db import models

# Create your models here.

class Ingredient(models.Model):
    name = models.CharField(max_length=50)

    def __str__(self):
        return self.name

    class Meta:
        db_table = 'ingredients'
        managed = True
        verbose_name = 'ingredients'
        verbose_name_plural = 'ingredients'


class Product(models.Model):
    typeMenu = (
        ('hamburgesa','hamburgesa'),
        ('pepitos','pepitos'),
        ('perros calientes','perros calientes'),
    )
    name = models.CharField(max_length=50)
    descriptions = models.TextField(null=True,blank=True)
    ingredient = models.ManyToManyField(Ingredient)
    category = models.CharField(choices = typeMenu,max_length=100)
    image = models.ImageField(upload_to='menu/product/',null=True,blank=True)
    price = models.IntegerField(null=True,blank=True)

    def __str__(self):
        return self.name

    class Meta:
        db_table = 'product'
        managed = True
        verbose_name = 'product'
        verbose_name_plural = 'products'

