from django.contrib import admin
from django.urls import path
from .views import ProductList,ProductDetail



urlpatterns = [
    path('ProductList/',ProductList.as_view(),name='ProductList'),
    path('ProductDetail/<int:pk>',ProductDetail.as_view(),name='ProductDetail'),

]
