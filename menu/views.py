from django.shortcuts import render
from django.views.generic import ListView,DetailView
from .models import Product



class ProductList(ListView):
    model = Product
    template_name = 'menu/menu.html'
    

class ProductDetail(DetailView):
    model = Product
    template_name = 'menu/detailProduct.html'



