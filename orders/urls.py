from django.contrib import admin
from django.urls import path, include
from  .views import process_order,styleMail



urlpatterns = [
    path('', process_order,name='Pedido'),
    path('styleMail/', styleMail,name='styleMail'),
]