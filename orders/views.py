from django.shortcuts import render
from django.contrib import messages
from django.core.mail import send_mail
from django.shortcuts import redirect
from django.contrib.auth.models import User
from django.template.loader import render_to_string
from django.utils.html import strip_tags
from django.views.generic import DetailView,ListView
from .models import Order,OrderLine
from cart.cart import Cart
from cart.context_processor import  importe_total
from django.contrib.auth.decorators import login_required

#creacion de pedidos 


@login_required(login_url='login')
def process_order(request):
    order = Order.objects.create(user=request.user,completed = True) #creamos un pedido pasandole el usuario y modificando la propiedad completado
    cart = Cart(request) #habilitamos el cart
    order_lines = list()#creamos una lista para recorrerla


    for key,value in cart.cart.items(): #recorremos el cart
        order_lines.append(
            OrderLine(
                product_id = key, #pasamos el id del producto
                quantity = value['quantity'], #pasamos la quantity
                user = request.user, #pasamos el usuario
                order = order #pasamos el pedido
            )
        ) #toda esta informacion se pasa al bluk_create

    OrderLine.objects.bulk_create(order_lines) #se pasa toda la informacion recopilada por la lista 'order_line'

    consultaTotal = importe_total(request)
    total = consultaTotal['importe_total']




    #validacion de mono minimo para envio al interior y tipo de entrega


    send_order_email( #aca se pasan los parameros a la funcion para enviar el correo
        order = order,
        order_lines = order_lines,
        username = request.user.username,
        user_email = request.user.email,
    )

    # Cart.clear()

    messages.success(request,'Pedido creado con exito, mira nuestras novedades ')
    cart.clear()
    
    return redirect(to='index')

def send_order_email(**kwargs):
    subject = 'Gracias por tu pedido' #asunto
    html_message = render_to_string('orders/envioEmails.html',{ #lo que va a contener el html
        'order':kwargs.get("order"),
        'order_lines': kwargs.get("order_lines"),
        'username':kwargs.get('username'),
    })

    plain_message = strip_tags(html_message) #se guarda en una variable para el cotexto de send mail
    from_email = 'Departamento de ventas de Haro Burgers' #quien envia el correo
    
    to = kwargs.get('user_email')# para quien se envia 


    send_mail(subject,plain_message,from_email,[to],html_message=html_message) #se pasa toda la info a la funcion que envia el pedido
    
def styleMail(request):
    return render(request,'orders/envioEmails.html')


