from django.contrib import admin
from django.urls import path,include
from django.contrib.auth import views as auth_views
from .views import registerUser,LogoutView

urlpatterns = [
    path('accounts/', include('django.contrib.auth.urls')),
    path('register/',registerUser,name='register'),
    path('logout/', LogoutView.as_view(next_page='login'), name='logout'),
    path('change-password/', auth_views.PasswordChangeView.as_view(template_name='users/changePassword.html'),name='changePassword'),
]
