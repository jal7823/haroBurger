from django.shortcuts import render
from django.shortcuts import redirect
from django.contrib.auth import authenticate,login
from django.contrib.auth.views import LoginView, LogoutView
from .forms import CustomUserCreationForms
# Create your views here.



def registerUser(request):
    data = {
        'form':CustomUserCreationForms()
    }

    if request.method == 'POST':
        form = CustomUserCreationForms(data=request.POST)
        if form.is_valid():
            form.save()
            user = authenticate(username=form.cleaned_data['username'],password=form.cleaned_data['password1'])
            login(request,user)
            return redirect(to='index')

        data['form'] = form       

    return render(request,'users/registrations.html',data)

class logout(LoginView):
    pass